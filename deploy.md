### Part 3 - Integrate the Watson assitant to the website
1. From Watson assistant you created in the previous task, choose `Integrate Web Chat`

<img src="images/begin_chatbot_integration.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

2. In the Web Chat screen that comes up, click on `Create`.

<img src="images/create_integration.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

3. Click on `Add an Avatar Image` to add a face to your virtual assitant. 
<img src="images/add_avatar_image.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

4. In the space provided for link, include a image URL as per the required dimensions or use [https://github.com/ibm-developer-skills-network/tilsj-petshop_webapp/raw/master/PetShoppePatty.png] and click on `Save`.
<img src="images/link_to_avatar_image.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

5. Change the name of the Virtual Assistant.

6. Change the accent colour to something you like for your chatbot.
<img src="images/accent_colour.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

7. Go to the 'Home screen` tab and add the initial dialog and converstion starters.<img src="images/conversation_starters.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

8. Click on the **embed** tab to view the integration script and copy it. 
<img src="images/copy_embed_script.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

9. Click on `Save and Exit` for the changes to be stored. 
<img src="images/save_and_exit.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>


10. Right click here on [index.html](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CB0106EN-SkillsNetwork/labs/FinalAssignment-coursera/data/index.html) and download it to your local machine.

11. Open index.html with notepad or other editors on you local machine.
    - Paste the **embed** script you copied in step 3, in the space provided in index.html.
    <img src="images/edit_indexhtml.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

    - Save the file
    
> <span title="camera">📷</span> **Take a screenshot of the index.html page with the changes and save it as **'edited_index.html'** in .jpg or .png format for the peer-graded assignment.(Please mask/blurr the **integrationID** and **serviceInstanceID** in the screenshot before submission)**


12. Open `index.html` in the browser by double-clicking on the html file from the file folder.


<img src="images/integrated chat.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Load the homepage and take a screenshot of the same showing the chatbot at the bottom right and save it as **'integrated_chat'** in .jpg or .png format for the peer-graded assignment.**

### Task 5 - Test the chatbot

Open the deployment URL and test the chatbot for the following: 

1. Retrieve a list of cats.

> <span title="camera">📷</span> **Take a screenshot of the chatbot showing the list of cats and save it as **'cats_list'** in .jpg or .png format for the peer-graded assignment.**

2. Retrieve a list of dogs.

> <span title="camera">📷</span> **Take a screenshot of the chatbot showing the list of dogs and save it as **'dogs_list'** in .jpg or .png format for the peer-graded assignment.**

3. Retrieve image and details of one selected pet.

> <span title="camera">📷</span> **Take a screenshot of the chatbot showing the details of the selected pet and save it as **'selected_pet_details'** in .jpg or .png format for the peer-graded assignment..**

# Congratulations!
You have completed the tasks for this project. In the peer assignement that follows, you will be required to upload the screenshots you saved in this lab.

## Authors
Lavanya

## Other Contributors
Shubham Yadhav

## Change Log

| Date (YYYY-MM-DD) | Version | Changed By        | Change Description                 |
| ----------------- | ------- | ----------------- | ---------------------------------- |
| 2021-06-25        | 1.0     | Lavanya | Created initial version of the lab |
