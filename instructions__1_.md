# Part A: Create Cloudant Services on IBM Cloud

**Estimated time needed:** 10 minutes

## Overview

In this lab, you will create a Cloudant service


### Create the Cloudant service

Navigate to [https://cloud.ibm.com/catalog](https://cloud.ibm.com/catalog) to launch the IBM Cloud Catalog.

From the catalog, choose the **Services** menu on the left, and filter by **Databases** and **Lite**. Then click on the **Cloudant** tile.

![1-catalog](images/4-catalog-cloudant.png){ width=1024 height=1024 }

On the next screen, choose the Cloudant offering, the multitenant environment, and the Lite tier. You can optionally give your service a name. Click the **Create** button to continue.

![1-catalog](images/13-cloudant-create.png){ width=1024 height=1024 }

The Cloudant service can take a few minutes to get provisioned. You will be taken to the **Resources** page. The **Status** will change to **Active** once the service has been provisioned.

![1-catalog](images/14-cloudant-provision.png){ width=1024 height=1024 }


**Congratulations!** You've successfully created the Cloudant service on IBM cloud!




### <h6 align="center"> © IBM Corporation 2020. All rights reserved. <h6/> <!-- omit in toc -->
