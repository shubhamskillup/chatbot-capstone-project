# Overview

You have to create a chatbot for a pet-store which iѕ a pet platform thаt gives opportunities fоr реtѕ lоvеrѕ to get the desired pet. This pet-store is to inѕрirе, рrоvоkе аnd еmроwеr реорlе tо get tо know more about реtѕ.

With the help of this platform anyone can connect to Adopt/buy a pet. Your work is to create a chatbot that can able to handle quires of people who wants to buy a pet from the pet-store or want to know more about them. This store provides other services such as **Pet Grooming** and **Vet Consultation** that you also include in your chatbot.

You will get the images and descriptions of the cats and dogs while doing Pre-requisite labs. This labs will make you familiar with the Cloudant technology and also gives you the basic Chatbot JSON file in which you will be performing the below project tasks.

You will also get one documentaion of chatbot integration using Twilio.

The Capstone has below several tasks that you must complete. 

1. **Populating Database & creating API endpoints**: In this task, you have to upload the data provided by us to the Cloudant database and then create an API to connect it to your chatbot.

2. **Chatbot updation:** In this task, you have to create the complete flow of the chatbot based on the given instructions in the further sections.

3. **Whatsapp Integration**: In this section, you have to integrate your chatbot with Whatsapp.

4. **Deploy your application**: Finally, in this section, you have to deploy your chatbot on a Website.

