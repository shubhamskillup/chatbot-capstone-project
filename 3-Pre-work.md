# Pre-Work

Some pre-work will be required for you to complete this Capstone successfully.

## Task1: IBM Cloud, IBM Cloudant & IBM Watson Assistant service creation

You will need an IBM Cloud account to do this Capstone. If you have not created one already, click on this [link](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/CreateIBMCloudAccount.md.html) and follow the instructions to create an IBM Cloud account.

Now, create the below services

1. **IBM Cloudant service**: Please click on this [link](https://gitlab.com/shubhamskillup/chatbot-capstone-project/-/blob/main/instructions__1_.md) to follow the instruction.

2. **IBM Watson Assistant service**: Please click on this [link](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CB0106EN-SkillsNetwork/labs/Module%201/Create_Watson_Assistant.md.html) to follow the instruction.


## Task2: Populate the Cloudant DB and API endpoints creation

You will need to add the data to your cloudant database, we have already created a dataset of pets(dog & cat) images and sort descriptions about them along with it, Now, after you are done with this you have to create APIs endpoints.

Please click [here](https://gitlab.com/shubhamskillup/chatbot-capstone-project/-/blob/main/peer-graded-assignment-instruction.md) to follow the instructions to perform the below tasks.

1. **Populate data into the cloudant db**

2. **Create API endpoints with Cloud Functions**

## Task3: Chatbot Creation Pre-Work

As you have populated the data and created the API Endpoints, Now, you need a chatbot to connect to your database. We have created a basic PetShop chatbot for you to work on, Please right clik [here](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CB0106EN-SkillsNetwork/labs/FinalAssignment-coursera/data/PetShop-Customer-Care-Skill.json), and choose `Save link as` to save `PetShop-Customer-Care-Skill.json` in your local directory and upload this skill to the IBM Watson Assistant.

Once you are done please click [here](https://gitlab.com/shubhamskillup/chatbot-capstone-project/-/blob/main/API_integration.md) to follow this instructions to perform **API integration with the chatbot**.

Now, your task is to improve the Chabot by creating the complete flow of it. You will find the project instruction in the next section.


