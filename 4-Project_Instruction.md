# Project Instruction

Till now you already have completed the Pre-Work of this Capstone and you have your dataset added to your Cloudent and have the basic working chatbot. In this section, you have to follow the given instructions and complete the end to end flow of chatbot to complete the Capstone.

We have divided the project instructions into three given parts:

# Part 1: Chatbot Updation

Now the database is loaded and connected to the chatbot. You have to create a complete flow of chatbot that can handles questions regarding **"Greeting"**, **"Appointment"**, **"Location"**, **"Thank you"**, **"Pets"** and **"Fallback"**.

The chatbot should able to handle all the below mentioned scenarios:

1. Chatbot should be able to respond to **hello** and **goodbye**.

2. Chatbot should be able to tell the **Services** provided by the store, that are **Pet Grooming**, **Vet Consultation**, and **Adopt/Buy Pet**.

3. Chatbot should be able to **book an appointment** for the services, which means if someone select any of these services the chatbot should ask a question for booking an appointment by giving them the option of **Yes** or **No**. 

4. Chatbot should collect the person's personal information like Name & Email while booking an appointment and also  ask about the date, time and location and all this information should gather by chatbot from a user within a single node, once it collects all the information then only the chatbot should book the appointment.
 > Note: The pet store is located in these four locations(**Calgary**, **Montreal**, **Toronto** & **Vancouver**) only, so if the person writes other locations then the chatbot should be able to recognize and answer that the stores are available for these given locations only, please choose between these locations only.

 5. Chatbot should be able to show  the available images of Dogs and Cats including a short description of them while asking about dogs/cats. 

 6. If the person selects the option **Pet Grooming** & **Vet Consultation** then it directs on the Appointment Node and asks for booking an appointment, but if the person selects the third option **Adopt/Buy Pet** then chatbot should ask that _**Are you looking for a Cat or a Dog?**_ and if it gets any of them in response it should show the images of pets and further proceed to book an appointment.

 7. If the person asks directly about the cat and dog then also the chatbot should be able to handle this query and reply to it. 

# Part 2: Whatsapp Integration

Integrate your chatbot with WhatsApp messaging so your assistant can exchange messages with the customers. You can automate the communication by deploying your Watson-powered chatbot to WhatsApp.

This integration creates a connection between your assistant and WhatsApp by using Twilio as a provider.

Hint: If you need any help, please click [here](https://gitlab.com/SrishtiiSrivastava/guided-projects/-/blob/main/WA_ChatbotIntegrationWithWhatsApp/Chatbot_integration_with_Whatsapp.md) to follow the instructions.

# Part 3: Deploy on a Website

Integrate your chatbot to the website. Please click [here](https://gitlab.com/shubhamskillup/chatbot-capstone-project/-/blob/main/deploy.md) to follow the instructions.






