# Introduction

In this capstone, you will apply your knowledge and expertise of Chatbot creation to a real world challenge. You will be using **IBM Cloud** account, the **Watson Assistant** and **Cloudant** service to create a chatbot for the **Pet Shop** company.

You will need to plan, implement, test and deploy chatbot that delight your user.

# Estimated time needed: 3 hours

# Scenario

You are a part of an AI team and the company has decided to use a virtual assistant on their PetShop website to enhance customer service. Their goal is to expand their business by reaching more people and increasing their customer base and also want to improve customer engagement by establishing personalized interactions with consumers, offering additional pet information, and the option to book an appointment.


## Use cases: 

1. 24/7 Customer Service
2. Less People-to-People Interactions with Customers
3. Checking out the pics of available animals
4. Checking the available Breed information
5. Book an appointment
6. Get the shop locations

# Review Criteria – 100 marks total

The capstone project is divided into 4 Tasks. Each task has followed by a final submission that is graded (by your peers in this course or by the instructors). The grading is divided as follows:

- **Task 1: Cloudant data Population and API Creation(20 points)**
- **Task 2: Chatbot creation (60 points)**
- **Task 3: Whatsapp Integration (10 points)**
- **Task 4: Deploy on a Website (10 points)**

Your capstone project is total of **100 points**.

## Next Steps
Be sure to read the capstone overview.

# Author(s)
<h4> Shubham Kumar Yadav <h4/>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-03-16 | 1.0 | Shubham Yadav | Created |

 

