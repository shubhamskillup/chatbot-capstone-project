### Task 3 - Create Watson Assistant that uses the API end points from the cloud function and the web application.



1. Once your chatbot is created, go to the functions and click on APIs, and copy the route.

<img src="images/5.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

2. Go to your petstore- customer_care_skill chatbot and click on **Options**, then go to Webhook and paste the API route which you copied in the earlier step.

<img src="images/6.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Take a screenshot of the webhook page where the URL is entered and save it as **'webhook_url'** in .jpg or .png format for the peer-graded assignment.(Please mask/blurr the URL part of the webhook_url screenshot before submission)**


3. Now to test your chatbot go to `Try it out` panel, on the top right. For your reference find the below screenshot.

<img src="images/9.PNG" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:50%"/>

<br/>

<img src="images/10.PNG" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:50%"/>

# Congratulations!
