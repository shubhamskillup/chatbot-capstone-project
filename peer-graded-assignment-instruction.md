## __Task1:__ Populate data into the cloudant db.

> *To run the commands in Cloud Shell, you can type or simply copy and paste in you IBM Cloud shell*

1. Open a new cloud shell. 
<img src="images/1-cloud-shell.png"  class="image_centre_75"/>

2. The IBM Cloud Shell terminal will be initialized. <img src="images/2-cloud-shell-terminal.png"  class=/>

    >This shell timesout when there is no activity. Any changes made will be lost.  

3. Run the following command to clone the repository. 

    ```
    [ ! -d 'tilsj-petshop_webapp' ] && git clone https://github.com/ibm-developer-skills-network/tilsj-petshop_webapp.git
    ```

4. Change to the `tilsj-petshop_webapp` folder. 

    ```
    cd tilsj-petshop_webapp
    ```

5. Install the packages required for the project  by running the following command

    ```
    pip3 install -r requirements.txt
    ```

6. Create a file `.env` under the current directory to map the apikey and username of the cloudant service noted in the pre-work session. 
    - Type `nano .env`. 
    - Add the credenetials as mentioned below. 
        ```
        account_name=<your username>
        apikey=<your apikey>
        ```
    - Once done, press `Ctrl+o` and enter to save. 
    - Press `Ctrl+x` to exit the nano editor. 
    <img src="images/env_nano_editor.png"  style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>
    
 
7. Run the following script to to populate the database. 
*This script connects to the Cloudant DB using the credentials provided in the .env file and populates data in it.*

    ```
    python3 addObjectsToCloudant.py
    ```
{: codeblock}
>This takes a few seconds. 

8. Check the cloudant database for the new documents you just added.
<img src="images/launch_db_dashboard.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

    *You should see a database named `pets-database` with 15 docs created.*

<img src="images/pets_database.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Take a screenshot of the Cloudant Database dashboard showing pets-database with 15 docs and save it as **'pets-database'** in .jpg or .png format for the peer-graded assignment.**


### Task 2 - API endpoints with Cloud Functions

1. On the IBM cloud [homepage](https://cloud.ibm.com/), go to the menu on the left and choose `functions` and then choose `Actions`.

2. Enter the name for the function, choose `python 3.7` for the runtime type and click on `Create`.

<img src="images/function_name.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

3. Remove the existing default content and paste the following code in the code space provided. Replace the cloud username and apikey with your credentials. 

```python
import sys
from ibmcloudant.cloudant_v1 import CloudantV1
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
def main(dict):
    databaseName = "pets-database"
    service_url = "https://apikey-v2-2fbmf63x6xo44u7xfpsejdvomi04eh4olv3699sf4f15:671cf3aaf1367d38a62e418a868df406@cc7d1ed4-5adc-4b06-b9b5-5405f4882ef0-bluemix.cloudantnosqldb.appdomain.cloud"
    apikey="3_8123R2ujttG90_i59V1joUYz-aalHagxXuo3Sem8Gn"
    authenticator = IAMAuthenticator(apikey)
    service = CloudantV1(authenticator=authenticator)
    service.set_service_url(service_url)
    req = dict['req']
    print(req)
    if (req == "getAllDetails"):
        kind = dict['kind']
        selector = {'kind': {'$eq': kind}}
        docs = service.post_find(db=databaseName,selector=selector).get_result()
        options = []
        for doc in docs["docs"]:
            pet_instance ={"label": str(doc["name"]+ " " + doc["age"] + " "+ doc["breed"]),"value": {"input":{"text":doc["_id"]}}}
            options.append(pet_instance)
        return { "generic":[
                    {"title":"Please choose one of these pets",
                    "options":options,
                            "description":"",
                            "response_type":"option"
                    }]}
    elif (req == "getPetDetails"):
        pet_id = dict['pet_id']
        selector = {'_id': {'$eq': pet_id}}
        docs = service.post_find(db=databaseName,selector=selector).get_result()
        pet_doc = None
        for doc in docs["docs"]:
            if(doc['_id'] == pet_id):
                pet_doc = doc
        pet_details = pet_doc['name']+" is a "+pet_doc['age']+" old "+pet_doc['breed']+". "+pet_doc['comments']
        return {"output":pet_details,"image_url":pet_doc['image_url']}
```
*This code connects to the Cloudant and retrieves the pet details. Depending on the parametes passed, it either retrieves all the pets of one kind or details of one specific pet.*

<details ><summary style="background-color:yellow">Click here for instructions to test the cloud function </summary>
Click on `Invoke with Parameters` and include the following JSON.

```json
{
    "req":"getAllDetails",
    "kind":"Dog"
}
```
Click on `Invoke`. You should see the output on the right side
</details>

4. Enable web-action for the function you just created. 

<img src="images/enable_webaction.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

5. Now that web-action is enabled, we create an API end-point that points to this function. Click on `Create API`.

<img src="images/create_api.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

6. Give it an `API name`. The base path for the API is automaticaly populated. Then click on `Create Operation`.

<img src="images/define_api_endpoint.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

7. A new window pops up where you can specify the path, method and the action you want to map the API to. Enter the details and click on `Create`.

<img src="images/endpoint_method_create.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

8. Now that the operation is created and you are able to see the newly created operation under the Operations list, you just need to scroll down to the bottom of the page, leaving the remaining values as default and click on `Create` to create an API end-point.

> <span title="camera">📷</span> **Take a screenshot of the API service page showing your newly created API and save it as **'functions-api'** in .jpg or .png format for the peer-graded assignment.**


# Congratulations!
You have completed this tasks.

## Authors
Lavanya

## Other Contributors
Shubham Yadhav

## Change Log

| Date (YYYY-MM-DD) | Version | Changed By        | Change Description                 |
| ----------------- | ------- | ----------------- | ---------------------------------- |
| 2021-06-25        | 1.0     | Lavanya | Created initial version of the lab |

 Copyright © 2020 IBM Corporation. All rights reserved.

